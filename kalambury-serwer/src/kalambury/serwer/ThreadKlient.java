/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalambury.serwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Dawid
 */
public class ThreadKlient implements Runnable {

    Klient klient;
    ArrayList<Klient> klienci;
    Rozgrywka rozgrywka;
    BufferedReader in;
    PrintWriter out;

    
    public ThreadKlient(Klient klient_, ArrayList<Klient> klienci_, Rozgrywka rozgrywka_) {
        
        klient = klient_;
        klienci = klienci_;
        rozgrywka = rozgrywka_;
        
        try {
            in = new BufferedReader(new InputStreamReader(klient.gniazdo.getInputStream()));
            out = new PrintWriter(klient.gniazdo.getOutputStream(), true);
        } catch (IOException e) {
        }
        
        String listaKlientow = Character.toString((char) 2);
        for (Klient biezacyKlient : klienci) {
            listaKlientow += biezacyKlient.nazwa + "," + biezacyKlient.punkty + ";";
        }
        wyslijDoWszystkich(listaKlientow);
    }
    

    public void wyslijListeKlientow() {
        String listaKlientow = Character.toString((char) 2);
        for (Klient biezacyKlient : klienci) {
            listaKlientow += biezacyKlient.nazwa + "," + biezacyKlient.punkty + ";";
        }
        wyslijDoWszystkich(listaKlientow);
    }

    
    public void wyslijDoWszystkich(String wiadomosc) {
        try {
            for (Klient biezacyKlient : klienci) {
                PrintWriter biezaceOut = new PrintWriter(biezacyKlient.gniazdo.getOutputStream(), true);
                biezaceOut.println(wiadomosc);
            }
        } catch (IOException e) {
        }
    }

    
    @Override
    public void run() {
        boolean otwarte = true;
        while (otwarte == true) {
            try {
                String wiadomosc = in.readLine();
                System.out.println("otrzymano " + wiadomosc);
                if (wiadomosc == null) {
                    otwarte = false;
                } else if (wiadomosc.getBytes()[0] == 1) { //wiadomosc tekstowa
                    wyslijDoWszystkich(wiadomosc);
                    if (rozgrywka.aktywna) { //jezeli rozgrywka jest aktywna to porownujemy z haslem
                        String[] wiadomoscSplit = wiadomosc.split(Character.toString((char) 2));
                        if (wiadomoscSplit[1].equals(rozgrywka.biezaceHaslo)) {
                            rozgrywka.koniecRundy(klient);
                            wyslijListeKlientow();
                        }
                    }
                } else if (wiadomosc.getBytes()[0] == 3) //informacje o rysunku
                {
                    wyslijDoWszystkich(wiadomosc);
                } else if (wiadomosc.getBytes()[0] == 's') //start
                {
                    System.out.println("Rozpoczynam rozgrywke");
                    wyslijDoWszystkich("s");
                    rozgrywka.start();
                    rozgrywka.nowaRunda();

                }
            } catch (IOException e) {
                otwarte = false;
            }
        }
        System.out.println("Rozlaczono z :" + klient.nazwa);
        klienci.remove(klient);
        if (rozgrywka.rysuje == klient) {
            rozgrywka.rysuje = null;
        }
        wyslijListeKlientow();
    }

}
