/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalambury.serwer;

import java.util.TimerTask;

/**
 *
 * @author Dawid
 */
public class RozgrywkaTimer extends TimerTask{
    
    private final Rozgrywka rozgrywka;
    
    public RozgrywkaTimer(Rozgrywka rozgrywka_){
        rozgrywka  = rozgrywka_;
    }

    @Override
    public void run() {
        rozgrywka.koniecCzasu();
    }
    
}
