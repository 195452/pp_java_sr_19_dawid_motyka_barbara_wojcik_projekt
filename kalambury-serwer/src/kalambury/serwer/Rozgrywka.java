/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalambury.serwer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Dawid
 */
public class Rozgrywka {

    public String[] hasla;
    public boolean aktywna;
    public int doIlu;
    public String biezaceHaslo;
    public Klient rysuje;
    private final int czasRundy;
    ArrayList<Klient> listaGraczy;
    Timer rozgrywkaTimer;

    public Rozgrywka(ArrayList<Klient> listaGraczy_, int doIlu_) {
        czasRundy = 40;
        NodeList nList;
        listaGraczy = listaGraczy_;
        aktywna = false;
        doIlu = doIlu_;
        File fXmlFile = new File("hasla.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            System.out.println(ex.toString());
        }

        org.w3c.dom.Document doc;
        try {
            doc = dBuilder.parse(fXmlFile);
            nList = doc.getElementsByTagName("haslo");
            hasla = new String[nList.getLength()];
            for (int i = 0; i < nList.getLength(); i++) {
                hasla[i] = nList.item(i).getTextContent();
            }
            
        } catch (SAXException | IOException ex) {
            System.out.println(ex.toString());
        }

    }

    public void wyslijDoWszystkich(String wiadomosc) {
        try {
            for (Klient biezacyKlient : listaGraczy) {
                PrintWriter biezaceOut = new PrintWriter(biezacyKlient.gniazdo.getOutputStream(), true);
                biezaceOut.println(wiadomosc);
            }
        } catch (IOException e) {
        }
    }

    public void zakoncz() {
        aktywna = false;
        rysuje = null;
        for (Klient biezacyKlient : listaGraczy) {
            biezacyKlient.punkty = 0;
            try {
                PrintWriter biezaceOut = new PrintWriter(biezacyKlient.gniazdo.getOutputStream(), true);
                biezaceOut.println('k'); //koniec
            } catch (IOException e) {
            }
        }
    }

    public void losuj() //sprawdza tez czy koniec
    {
        ListIterator<Klient> itrRysuje;
        if (rysuje != null) {
            itrRysuje = listaGraczy.listIterator(listaGraczy.indexOf(rysuje));
            itrRysuje.next();
            if (itrRysuje.hasNext()) {
                rysuje = itrRysuje.next();
            } else {
                itrRysuje = listaGraczy.listIterator();
                rysuje = itrRysuje.next();
            }
        } else {
            itrRysuje = listaGraczy.listIterator();
            rysuje = itrRysuje.next();
        }

        biezaceHaslo = hasla[(int) (Math.random() * (hasla.length - 1))];
    }

    public void koniecRundy(Klient wygral) {
        rozgrywkaTimer.cancel();
        rysuje.punkty += 20;
        wygral.punkty += 10;

        wyslijDoWszystkich(Character.toString((char) 1) + "Runda zakonczona, wygral " + wygral.nazwa);

        if (rysuje.punkty >= doIlu || wygral.punkty >= doIlu) {
            if (rysuje.punkty >= doIlu) {
                wyslijDoWszystkich(Character.toString((char) 1) + "Rozgrywka zakonczona, wygral " + rysuje.nazwa);
            } else {
                wyslijDoWszystkich(Character.toString((char) 1) + "Rozgrywka zakonczona, wygral " + wygral.nazwa);
            }
            zakoncz();

        } else {
            nowaRunda();
        }

    }

    public void koniecCzasu() {
        wyslijDoWszystkich(Character.toString((char) 1) + "Runda zakonczona, nikt nie odgadl hasla");
        nowaRunda();
    }

    public void nowaRunda() {
        losuj();
        try {
            for (Klient biezacyGracz : listaGraczy) { //do kazdego wysylanie informacji kto rysuje
                PrintWriter out = new PrintWriter(biezacyGracz.gniazdo.getOutputStream(), true);
                if (biezacyGracz.equals(rysuje)) {
                    out.println("" + 'r' + biezaceHaslo); //rysuje + haslo
                    out.println(Character.toString((char) 1) + "Nowa runda, ty rysujesz. Haslo: " + biezaceHaslo);
                } else {
                    out.println("" + 'z' + rysuje.nazwa); //zgaduje + kto rysyje
                    out.println(Character.toString((char) 1) + "Nowa runda, rysuje " + rysuje.nazwa);
                }
            }
        } catch (IOException e) {
        }
        rozgrywkaTimer = new Timer();
        rozgrywkaTimer.schedule(new RozgrywkaTimer(this), czasRundy * 1000);
    }

    public void start() {
        aktywna = true;

    }
}
