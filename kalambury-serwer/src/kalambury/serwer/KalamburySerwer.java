/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalambury.serwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Dawid
 */
public class KalamburySerwer {

    public static ArrayList<Klient> klienci = new ArrayList<>();
    public static Rozgrywka rozgrywka;
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        rozgrywka = new Rozgrywka(klienci, 300);
        
        ServerSocket serverSocket = new ServerSocket(50000);
        while (true) {
            Socket clientSocket = serverSocket.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            Klient nowyKlient = new Klient();
            nowyKlient.nazwa = in.readLine();
            nowyKlient.gniazdo = clientSocket;
            klienci.add(nowyKlient);
            System.out.println("Nowy uzytkownik: " + nowyKlient.nazwa);
            Thread klientThread = new Thread(new ThreadKlient(nowyKlient, klienci,rozgrywka));
            klientThread.start();
        }

    }

}
