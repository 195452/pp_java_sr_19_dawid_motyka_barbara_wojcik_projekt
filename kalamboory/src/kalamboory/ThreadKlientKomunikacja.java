/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalamboory;

import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dawid
 */
public class ThreadKlientKomunikacja implements Runnable {

    Socket gniazdo;
    static DefaultTableModel dtm;
    JTextArea jTextAreaWiadomosci;
    Graphics grafika;
    FrameKlientKalambury.CzyRysuje czyRysuje;
    JButton jButtonStart;
    JLabel jLabelStanRozgrywki;

    public ThreadKlientKomunikacja(Socket gniazdo_, DefaultTableModel dtm_, 
            JTextArea jTextAreaWiadomosci_, Graphics grafika_, 
            FrameKlientKalambury.CzyRysuje czyRysuje_,
            JButton jButtonStart_, JLabel jLabelStanRozgrywki_) {
        jButtonStart = jButtonStart_;
        jLabelStanRozgrywki = jLabelStanRozgrywki_;
        czyRysuje = czyRysuje_;
        grafika = grafika_;
        gniazdo = gniazdo_;
        dtm = dtm_;
        jTextAreaWiadomosci = jTextAreaWiadomosci_;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(gniazdo.getInputStream()));
            while (true) {
                String odpowiedz = in.readLine();
                byte odpowiedzPierwszyZnak = odpowiedz.getBytes()[0];
                if (odpowiedzPierwszyZnak == 1) {
                    odpowiedz = odpowiedz.substring(1);
                    System.out.println("wiadomosc: " + odpowiedz);
                    String[] wiadomoscSplit = odpowiedz.split(Character.toString((char) 2));
                    if (wiadomoscSplit.length > 1) {
                        jTextAreaWiadomosci.append(wiadomoscSplit[0] + ": " + wiadomoscSplit[1] + "\n");

                    } else {
                        jTextAreaWiadomosci.append(odpowiedz + "\n");
                    }
                    jTextAreaWiadomosci.setCaretPosition(jTextAreaWiadomosci.getDocument().getLength());

                }
                else if (odpowiedzPierwszyZnak == 2) { //lista graczy
                    odpowiedz = odpowiedz.substring(1);
                    while (dtm.getRowCount() > 0) {
                        dtm.removeRow(0);
                    }
                    System.out.println("lista: " + odpowiedz);
                    String[] odpowiedzUzytkownicy = odpowiedz.split(";");
                    for (String uzytkownik : odpowiedzUzytkownicy) {
                        String[] uzytkownikSzczegoly = uzytkownik.split(",");
                        dtm.addRow(new String[]{uzytkownikSzczegoly[0], uzytkownikSzczegoly[1]});
                    }
                }
                else if (odpowiedzPierwszyZnak == 3) { //dane do rysowania
                    odpowiedz = odpowiedz.substring(1);
                    StringReader odpowiedzReader = new StringReader(odpowiedz);
                    String[] wspolrzedne = odpowiedz.split(",");
                    grafika.drawLine(Integer.parseInt(wspolrzedne[0]), Integer.parseInt(wspolrzedne[1]), Integer.parseInt(wspolrzedne[2]), Integer.parseInt(wspolrzedne[3]));
                } else if (odpowiedzPierwszyZnak=='s') //start rozgrywki
                {
                    jButtonStart.setEnabled(false);
                    jButtonStart.setText("Gra w trakcie...");
                }
                else if (odpowiedzPierwszyZnak=='r')
                {
                    czyRysuje.rysuje=true;
                    odpowiedz = odpowiedz.substring(1);
                    jLabelStanRozgrywki.setText("Rysujesz, haslo: " + odpowiedz);
                    grafika.clearRect(0, 0, 999, 999);
                    
                } else if (odpowiedzPierwszyZnak=='z')
                {
                    czyRysuje.rysuje=false;
                    odpowiedz = odpowiedz.substring(1);
                    jLabelStanRozgrywki.setText("Rysuje: " + odpowiedz);
                    grafika.clearRect(0, 0, 999, 999);
                } else if (odpowiedzPierwszyZnak=='k')
                {
                    czyRysuje.rysuje=false;
                    jLabelStanRozgrywki.setText("Oczekiwanie na rozpoczecie" );
                    jButtonStart.setEnabled(true);
                    jButtonStart.setText("Rozpocznij gre");
                }

            }
        } catch (IOException e) {
            //System.out.print(e);
        }

    }
}
